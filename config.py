import os 

def get_path_database()-> str:
    path_db = os.path.join(os.path.dirname(os.path.realpath(__file__)), "database")
    if not os.path.exists(path_db):
        os.mkdir(path_db)
    return path_db

os.environ['DATABASE'] = get_path_database()
