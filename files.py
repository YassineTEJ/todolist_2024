import json
import os

class File:
    def __init__(self) -> None:
        self.path_database = os.environ.get('DATABASE')

    def _check_file(self, filename: str) -> bool:
        """Vérifier si le fichier existe."""
        return os.path.exists(self._join_path(filename))
        
    def _join_path(self, filename: str) -> str:
        """Joindre le chemin de base au nom du fichier."""
        return os.path.join(self.path_database, filename)

    def write(self, filename: str, **kwargs) -> bool:
        """Écrire des données dans un fichier JSON. Retourne False si le fichier existe déjà."""
        filepath = self._join_path(filename)
        if os.path.exists(filepath):
            return False  
        try:
            with open(filepath, 'w') as file:
                json.dump(kwargs, file, indent=4)
            return True
        except IOError as e:
            raise Exception(f"Erreur d'écriture dans le fichier {filename}: {e}")

    def read(self, filename: str) -> dict:
        """Lire et retourner le contenu d'un fichier JSON."""
        filepath = self._join_path(filename)
        if not self._check_file(filepath):
            raise FileNotFoundError(f"Le fichier {filename} n'existe pas.")
        try:
            with open(filepath, 'r') as file:
                return json.load(file)
        except (IOError, json.JSONDecodeError) as e:
            raise Exception(f"Erreur lors de la lecture du fichier {filename}: {e}")
        
    def update(self, filename: str, **kwargs) -> bool:
        """Mettre à jour des données spécifiques dans un fichier JSON."""
        filepath = self._join_path(filename)
        if not self._check_file(filepath):
            return False

        try:
            with open(filepath, 'r') as file:
                json_data = json.load(file)

            json_data.update(kwargs)

            with open(filepath, 'w') as file:
                json.dump(json_data, file, indent=4)

            return True
        except (IOError, json.JSONDecodeError) as e:
            raise Exception(f"Erreur lors de la mise à jour du fichier {filename}: {e}")

    def delete(self, filename: str) -> bool:
        """Supprimer un fichier."""
        filepath = self._join_path(filename)
        if not self._check_file(filepath):
            return False
        try:
            os.remove(filepath)
            return True
        except Exception as e:
            raise Exception(f"Erreur lors de la suppression du fichier {filename}: {e}")
