import utils.utils as utils
import files

class Todo:
    def __init__(self, user=None) -> None:
        self.user = user if user is not None else self.connection()
        self.file_manager = files.File()
        self._form_data = {'filename': ''}
        self._default_form_data = {'title': '', 'content': ''}

    def connection(self):
        utils.render("Who are you?")
        while True:
            name = utils.enter('Enter your name:').strip()
            if name:
                return name
            else:
                utils.render('ERROR: Name cannot be empty.')

    def _reset_form_data(self):
        self._form_data = {'filename': ''}
        self._default_form_data = {'title': '', 'content': ''}

    def insert_form_data(self, DEFAULT: bool = True):
        if DEFAULT:
            for default in self._default_form_data:
                value = utils.enter(f'Enter value for {default}:').strip()
                if value:
                    self._default_form_data[default] = value
        else:
            while True:
                key = utils.enter("Enter key (or type 'exit' to stop):").strip()
                if key == 'exit':
                    break
                if key:
                    value = utils.enter(f'Enter value for {key}:').strip()
                    if value:
                        self._form_data[key] = value

    def create_todo(self):
        while True:
            default_choice = utils.enter('Use default configuration? (y/n):').strip().lower()
            if default_choice in ['y', 'n']:
                break
            else:
                utils.render('Please enter "y" for yes or "n" for no.')

        self.insert_form_data(DEFAULT=(default_choice == 'y'))
        
        filename = self._form_data.get('filename', '').strip()
        if filename:
            filename += ".json"
        self._form_data['filename'] = filename

        try:
            self.file_manager.write(**self._form_data)
        except Exception as e:
            utils.render(f"An error occurred while writing the file: {e}")
        finally:
            self._reset_form_data()

    def read_todo(self, filename):
        try:
            return self.file_manager.read(filename)
        except Exception as e:
            utils.render(f"An error occurred while reading the file: {e}")

    def update_todo(self, filename, data):
        try:
            return self.file_manager.update(filename, **data)
        except Exception as e:
            utils.render(f"An error occurred while updating the file: {e}")

    def delete_todo(self, filename):
        try:
            return self.file_manager.delete(filename)
        except Exception as e:
            utils.render(f"An error occurred while deleting the file: {e}")

# Utilisation de la classe Todo
todo = Todo()
print(f"Connected as: {todo.user}")
