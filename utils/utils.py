import time
import sys
import os

def clear():
    if os.name == 'nt':
        os.system('cls')
    else:
        os.system('clear')

def _progress_bar_constructor(progress, total):
    percent = 100 * (progress / float(total))
    bar = '█' * int(percent) + '-' * (100 - int(percent))
    sys.stdout.write('\r|{bar}| {percent:.2f}%'.format(bar=bar, percent=percent))
    sys.stdout.flush()

def progress_bar(nb_iter:int=100):
    # Nombre total d'itérations pour simuler un travail
    total = nb_iter
    for i in range(total + 1):
        time.sleep(0.1)  # Simule un travail réel en cours
        _progress_bar_constructor(i, total)
    print()

def render(*args):
    print(*args)

def enter(msg:str):
    return input(f"{msg}\n# ")
